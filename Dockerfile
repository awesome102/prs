# Use an official Golang runtime as a parent image
FROM golang:1.18.4

# Set the working directory to /admin
WORKDIR /prs/cmd

# Set up a Go module-aware GOPATH

ENV GOPROXY=direct
ENV GOPATH=/go
ENV GOPRIVATE="https://gitlab.com/prs"
ENV CI_USER_NAME="v.seleckij"

# Copy the Golang code from the src directory to the container's /admin directory
COPY . .

# Install any needed dependencies
#RUN apt-get update && apt-get install -y git
#RUN git config --global user.email "vlad_sl@ukr.net"
#RUN git config --global user.name "vladichka"
#RUN git config --global http.extraHeader "PRIVATE-TOKEN: $CI_JOB_TOKEN" && \
#RUN    git clone https://gitlab.com/awesome102/prs.git ./go/src/prs && \
#    cd ./go/src/prs && \
#    git checkout dev --quiet && \
#    git submodule update --init --recursive



# Build the Go app

RUN go build ./cmd/main.go

EXPOSE 8080
# Define the command to run the executable
CMD ["./main"]
