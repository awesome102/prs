// // const popupButton = $("#add__img")
// //todo https://www.youtube.com/watch?v=5vx2PVClSVU
// var data = [];
// for (var i = 0; i < 100000; i++) {
//     var tmp = [];
//     for (var i = 0; i < 100000; i++) {
//         tmp[i] = 'hue';
//     }
//     data[i] = tmp;
// };
// const optionsForUpload = {
//     multiple : true, //мультивібор
//     accepts: [".png", ".jpeg", ".jpg"],
//     onUpload(files){
//
//
//         files.forEach(file =>{
//
//             let sendData = new FormData()
//             sendData.append("imgfile",file)
//
//
//             $.ajax({
//
//                 url: '/api/service/uploader/one',
//                 type: 'POST',
//                 enctype:'multipart/form-data',
//                 data: sendData,
//                 timeout: 15000,
//                 cache: false,
//                 processData: false,
//                 contentType:false,
//                 // cache:false,
//                 start_time: new Date().getTime(),
//                 headers: {
//                     "device": web,
//                     "language": "UK",
//                     "organizatorID":window.organizatorID,
//                 },
//                 complete: function() {
//
//                     // alert('This request took '+(new Date().getTime() - this.start_time)+' ms');
//                 },
//                 success: function (){
//                     toastr.success("Успішно завантажено")
//                 },
//                 error: function () {
//                     toastr.error("Не вдалося завантажити")
//                 }
//         })
//             // calculate_load_times()
//             // const observer = new PerformanceObserver((list) => {
//             //     list.getEntries().forEach((entry) => {
//             //         const request = entry.responseStart - entry.requestStart;
//             //         if (request > 0) {
//             //             console.log(`${entry.name}: Request time: ${request}ms`);
//             //         }
//             //     });
//             // });
//             //
//             // observer.observe({ type: "resource", buffered: true });
//
//     })
// },
// }
// var check =[]
// let upl = true
// $("#add__img").click(function () {
//     $("#popup").addClass("open")
//     // $("#popup").css("overflow","hidden")
//     $("html").css("overflow","hidden")
//     $('body,html').animate({ scrollTop: 0}, 800)
//     upload("#file",optionsForUpload)
//     })
// $("#popup__close").click(function () {
//     $("#popup").removeClass("open")
//     $("html").css("overflow","scroll")
//     //остановка функции upload
//     uplStop()
//
// })
//
// function noop(){}
// //создание html елементов
// const  element = (tag, classes= any = [],content,type) => {
//     const node = document.createElement(tag)
//
//     if (classes.length){
//         node.classList.add(...classes)
//     }
//     if (content){
//         node.textContent = content
//     }
//     if (type){
//         node.type = type
//     }
//     return node
// }
// function upload(selector, options={}){
//     //stop upload if was closed popup
//     if(!upl) return false;
//     let files = []
//     const onUpload = options.onUpload ?? noop
//     const input = document.querySelector(selector)
//     const open = element('button', ['btn__open'],"Відкрити", 'button' )
//     const upload = element('button',['btn__upload', 'primary'],"Завантажити", 'button' )
//     const preview = element('div', ['preview'])
//     upload.style.display='none'
//
//     //добавляем отображение картинок на фронт
//     input.insertAdjacentElement('afterend', preview)
//     //add button upload
//     input.insertAdjacentElement('afterend', upload)
//     //добавляем кнопку открыть
//     input.insertAdjacentElement("afterend", open)
//
//
//     if (options.multiple){
//         //включаем мультивыбор
//         input.setAttribute('multiple', true)
//     }
//     if (options.accepts && Array.isArray(options.accepts)){
//         //включаем провреку загружаемый файлов на формат + загрузочное окно блочит не верный формат.
//         input.setAttribute('accept',options.accepts.join(','))
//     }
//     //отслеживаем нажатия на кнопки
//     const triggerInput = () => input.click()
//     const clearPreview = el =>{
//         el.style.bottom = '4px'
//         el.innerHTML = '<div class="preview-info-progress"></div>'
//         progressBar()
//     }
//     const changeHandler = event =>{
//         upload.style.display = 'inline'
//         //добавляем загруженніе фотки в слайс для проверок
//         check.push(event.target.files.length)
//         //если загруженніх елементов больше 10, очищаем список.??????ї
//         //todo посмотреть нужно ли єто вообще
//         check.filter(function (number) {
//             if (number >= 10){
//                 check.splice(-1)
//             }
//         })
//         //если пустая форма
//         if (!event.target.files.length){
//             return
//         }
//         //если в форме больше 10
//         if (event.target.files.length > 10){
//             callAlertErrorNotification("max 10 img")
//             return
//         }
//         //проверка загруженніх файлов, очищаем результат.
//         if (checker(check) > 10){
//             check.splice(-1)
//             return callAlertErrorNotification("max 10 img")
//         }
//
//
//         files = Array.from(event.target.files)
//         //для каждой картинки оборачиваем ее в теги, загружаем в ридер и пушим на фронт
//         files.forEach(file =>{
//             if (!file.type.match("image")){
//                 return
//             }
//             const reader = new FileReader()
//             reader.onload = ev =>{
//                 const src = ev.target.result
//                 preview.insertAdjacentHTML("afterbegin",`
//                 <div class="preview-image">
//                     <div class="preview-close" data-name="${file.name}">&times;</div>
//                     <img src="${src}"alt="${file.name}"/>
//                     <div class="preview-info">
//                         <span>${file.name}</span>
//                         ${bytesToSize(file.size)}
//                     </div>
//                 </div>
//                 `)
//             }
//             reader.readAsDataURL(file)
//
//         })
//
//
//
//     }
//     const removeHandler = event => {
//         if (!event.target.dataset.name){
//         }
//
//         const {name}= event.target.dataset
//         files = files.filter(file => file.name !== name)
//         if (!files.length){
//             upload.style.display = 'none'
//         }
//         const block = preview.querySelector(`[data-name="${name}"]`).closest('.preview-image')
//         block.classList.add('removing')
//         setTimeout(()=>block.remove(), 300)
//
//     }
//     const uploadHandler = () => {
//         preview.querySelectorAll('.preview-remove').forEach(e => e.remove())
//         const previewInfo = preview.querySelectorAll('.preview-info')
//         previewInfo.forEach(clearPreview)
//         onUpload(files)
//     }
//     //подмена кнопки
//     open.addEventListener("click", function (e) {
//         triggerInput()
//         e.preventDefault()
//     })
//     //обработка файла
//     input.addEventListener("change",changeHandler)
//     //удаление елементов в превью
//     preview.addEventListener('click', removeHandler)
//     //загрузка файлов
//     upload.addEventListener('click',uploadHandler)
//
//
// }
// function bytesToSize(bytes) {
//     const sizes = ['Bytes', 'KB', 'MB', 'GB', 'TB']
//     if (!bytes) {
//         return '0 Byte'
//     }
//     const i = parseInt(Math.floor(Math.log(bytes) / Math.log(1024)))
//     return Math.round(bytes / Math.pow(1024, i)) + ' ' + sizes[i]
// }
// function checker(array) {
//     return array.reduce((a, b) => {
//         return a + b;
//     }, 0)
// }
// function uplStop(){
//     upl = false
// }
//
//
// function progressBar() {
//     var i = 0;
//     if (i === 0) {
//         i = 1;
//         var elem = $(".preview-info-progress");
//         var width = 1;
//         var id = setInterval(frame, 10);
//         function frame() {
//             if (width >= 100) {
//                 clearInterval(id);
//                 i = 0;
//             } else {
//                 width++;
//                 elem.css("width",width+"%");
//                 elem.html(width+'%')
//             }
//         }
//     }
// }

function calculate_load_times() {
    // Check performance support
    if (performance === undefined) {
        console.log("= Calculate Load Times: performance NOT supported");
        return;
    }

    // Get a list of "resource" performance entries
    const resources = performance.getEntriesByType("resource");
    if (resources === undefined || resources.length <= 0) {
        console.log("= Calculate Load Times: there are NO `resource` performance records");
        return;
    }

    console.log("= Calculate Load Times");
    resources.forEach((resource, i) => {
        console.log(`== Resource[${i}] - ${resource.name}`);
        // Redirect time
        let t = resource.redirectEnd - resource.redirectStart;
        console.log(`… Redirect time = ${t}`);

        // DNS time
        t = resource.domainLookupEnd - resource.domainLookupStart;
        console.log(`… DNS lookup time = ${t}`);

        // TCP handshake time
        t = resource.connectEnd - resource.connectStart;
        console.log(`… TCP time = ${t}`);

        // Secure connection time
        t = (resource.secureConnectionStart > 0) ? (resource.connectEnd - resource.secureConnectionStart) : "0";
        console.log(`… Secure connection time = ${t}`);

        // Response time
        t = resource.responseEnd - resource.responseStart;
        console.log(`… Response time = ${t}`);

        // Fetch until response end
        t = (resource.fetchStart > 0) ? (resource.responseEnd - resource.fetchStart) : "0";
        console.log(`… Fetch until response end time = ${t}`);

        // Request start until response end
        t = (resource.requestStart > 0) ? (resource.responseEnd - resource.requestStart) : "0";
        console.log(`… Request start until response end time = ${t}`);

        // Start until response end
        t = (resource.startTime > 0) ? (resource.responseEnd - resource.startTime) : "0";
        console.log(`… Start until response end time = ${t}`);
    });
}


