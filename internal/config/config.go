package config

type (
	Telegram struct {
		Token string `env:"TOKEN" envDefault:"BookingSportFacilitiesDefault"`
	}

	BinanceAPI struct {
		URL string `env:"BINANCE_URL,required" envDefault:"binance.com"`
	}
	OkxAPI struct {
		URL string `env:"OKX_URL,required" envDefault:"okx.com"`
	}
	ExternalServices struct {
		Host string `env:"LOCALIZATION_HOST" envDefault:"localhost"`
		Port string `env:"LOCALIZATION_PORT" envDefault:"8820"`
	}
)

type Config struct {
	Telegram         Telegram
	Binance          BinanceAPI
	Okx              OkxAPI
	ExternalServices ExternalServices
}
