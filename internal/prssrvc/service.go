package prssrvc

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"regexp"
	"strings"

	"github.com/PuerkitoBio/goquery"
	json "github.com/json-iterator/go"
)

func NewParserGoQueryOkx(url string) {
	client := http.Client{}
	req, err := http.NewRequest("GET", url, nil)
	if err != nil {
		fmt.Println(err)
	}
	req.Header = http.Header{
		"accept":                    {"text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.9"},
		"accept-encoding":           {"ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"},
		"cache-control":             {"max-age=0"},
		"cookie":                    {"first_ref=https%3A%2F%2Fwww.google.com%2F; okg.currentMedia=xl; _ga=GA1.1.1794925738.1673276724; locale=uk_UA; OptanonAlertBoxClosed=2023-01-09T15:05:43.319Z; OptanonConsent=isGpcEnabled=0&datestamp=Mon+Jan+09+2023+17%3A05%3A43+GMT%2B0200+(%D0%92%D0%BE%D1%81%D1%82%D0%BE%D1%87%D0%BD%D0%B0%D1%8F+%D0%95%D0%B2%D1%80%D0%BE%D0%BF%D0%B0%2C+%D1%81%D1%82%D0%B0%D0%BD%D0%B4%D0%B0%D1%80%D1%82%D0%BD%D0%BE%D0%B5+%D0%B2%D1%80%D0%B5%D0%BC%D1%8F)&version=202212.1.0&isIABGlobal=false&hosts=&consentId=a8ca0dd8-f17c-4ce6-ae7e-1f0f0f844878&interactionCount=2&landingPath=NotLandingPage&groups=C0004%3A0%2CC0002%3A0%2CC0001%3A1&AwaitingReconsent=false; _ga_G0EKWWQGTZ=GS1.1.1673276724.1.1.1673276753.31.0.0; __cf_bm=TF0rXDORMNdczGR_wpQhlrvUsW6iaV_b_QAAro02yXY-1673278523-0-AaPopTHglzPy7dtUu3EnQn8pqNNEpLS+QZdL+2m77g7bxGh9PscMgUYU7dG6N69hSC0n69WXyDYLz8y/4cDJGOM=; _monitor_extras={\"deviceId\":\"mSnevUN6fAwLUp-RSxmSs5\",\"eventId\":14,\"sequenceNumber\":14"},
		"referer":                   {"https://www.google.com/"},
		"sec-ch-ua":                 {"\"Google Chrome\";v=\"107\", \"Chromium\";v=\"107\", \"Not=A?Brand\";v=\"24\""},
		"sec-ch-ua-mobile":          {"?0"},
		"sec-ch-ua-platform":        {"Linux"},
		"sec-fetch-dest":            {"document"},
		"sec-fetch-mode":            {"navigate"},
		"sec-fetch-site":            {"same-origin"},
		"sec-fetch-user":            {"?1"},
		"upgrade-insecure-requests": {"1"},
		"user-agent":                {"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"},
	}
	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	if res.StatusCode != 200 {
		fmt.Sprintf("status code error: %d %s", res.StatusCode, res.Status)
	}
	doc, err := goquery.NewDocumentFromReader(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	var data []PairOkx
	re := regexp.MustCompile(`UAH.?`)
	doc.Find("tbody tr").Each(func(i int, selection *goquery.Selection) {
		selectorPrice := selection.Find(".price").Text()
		price := strings.ReplaceAll(selectorPrice, "UAH", "")
		currency := string(re.Find([]byte(selectorPrice)))
		data = append(data, PairOkx{Price: price, Currency: currency})
	})
	fmt.Println(data)
}
func NewParserGoQueryBinance(url string, data InputForUsers) {
	input := OutHeadersForBinance{
		Asset:          data.Asset,
		Countries:      data.Countries,
		Fiat:           data.Fiat,
		Page:           data.Page,
		PayTypes:       data.PayTypes,
		ProMerchantAds: data.ProMerchantAds,
		Rows:           10,
		TradeType:      data.TradeType,
	}

	fmt.Print(input)
	bd, err := json.Marshal(input)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Printf("%s\n", bd)
	client := http.Client{}
	req, err := http.NewRequest("POST", url, bytes.NewReader(bd))
	if err != nil {
		fmt.Println(err)
	}
	req.Header = http.Header{
		"accept":          {"*/*"},
		"accept-encoding": {"gzip, deflate, br"},
		"accept-language": {"ru-RU,ru;q=0.9,en-US;q=0.8,en;q=0.7"},
		"content-type":    {"application/json"},
		"user-agent":      {"Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/107.0.0.0 Safari/537.36"},
	}

	res, err := client.Do(req)
	if err != nil {
		fmt.Println(err)
	}
	fmt.Println(res.StatusCode)
	if res.StatusCode != 200 {
		fmt.Sprintf("status code error: %d %s", res.StatusCode, res.Status)
	}
	s, err := io.ReadAll(res.Body)
	if err != nil {
		fmt.Println(err)
	}
	//fmt.Println(string(s))
	responseFromBinance := Response{}
	if err := json.Unmarshal(s, &responseFromBinance); err != nil {
		fmt.Println(err)
	}
	var lot []PairBinance
	for k, _ := range responseFromBinance.Data {
		lot = append(lot, PairBinance{NickName: responseFromBinance.Data[k].Advertiser.NickName,
			Price:               responseFromBinance.Data[k].Adv.Price,
			FiatSymbol:          responseFromBinance.Data[k].Adv.FiatSymbol,
			Fiat:                responseFromBinance.Data[k].Adv.FiatUnit,
			MaxTradableQuantity: responseFromBinance.Data[k].Adv.DynamicMaxSingleTransAmount,
			MinTradableQuantity: responseFromBinance.Data[k].Adv.MaxSingleTransAmount,
			Available:           responseFromBinance.Data[k].Adv.TradableQuantity,

			MonthOrderCount: responseFromBinance.Data[k].Advertiser.MonthOrderCount,
			MonthUserRate:   responseFromBinance.Data[k].Advertiser.MonthFinishRate,
		})
		for key, _ := range responseFromBinance.Data[k].Adv.TradeMethods {
			lot[k].TradeMethod = append(lot[k].TradeMethod, responseFromBinance.Data[k].Adv.TradeMethods[key].TradeMethodName)
		}

	}
	fmt.Println(lot)
	//fmt.Println(responseFromBinance.Data[1].Advertiser.NickName)
	//fmt.Println(responseFromBinance.Data[1].Adv.FiatSymbol)
	//fmt.Println(responseFromBinance.Data[1].Adv.Price)

	//doc, err := goquery.NewDocumentFromReader(res.Body)
	//if err != nil {
	//	fmt.Println(err)
	//}
	//var data []pair
	//re := regexp.MustCompile(`UAH.?`)
	//fmt.Println(doc.Find(".css-1m1f8hn").Text())
	//doc.Find("div").Each(func(i int, selection *goquery.Selection) {
	//	fmt.Println(selection.Find("").Text())
	//})
	//fmt.Println(data)
}

//func NewParserCollyBinance(url string) {
//	c := colly.NewCollector(colly.AllowedDomains("p2p.binance.com"))
//	fmt.Println(c)
//	//curr := pair{}
//
//	c.OnResponse(func(response *colly.Response) {
//		fmt.Println(response.StatusCode)
//	})
//
//	c.OnRequest(func(r *colly.Request) {
//
//		fmt.Println("Visiting", r.URL)
//	})
//	c.OnHTML("div [data-tutorial-id=trade_price_limit]", func(element *colly.HTMLElement) {
//		fmt.Println(element)
//		//curr.price = element.ChildText("div.css-16jkuzw")
//		//fmt.Println(curr.price)
//	})
//
//	c.OnError(func(r *colly.Response, err error) {
//		fmt.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
//	})
//	err := c.Visit(url)
//	if err != nil {
//		fmt.Println("Error Visiting", zap.Error(err))
//	}
//}
//func NewParserCollyOkx(url string) {
//	c := colly.NewCollector(colly.AllowedDomains("okx.com", "www.okx.com"))
//	fmt.Println(c)
//
//	c.OnResponse(func(response *colly.Response) {
//		fmt.Println(response.StatusCode)
//
//	})
//	c.OnHTML("tbody tr", func(element *colly.HTMLElement) {
//		fmt.Println(element)
//
//	})
//	c.OnRequest(func(r *colly.Request) {
//		fmt.Println("Visiting", r.URL)
//	})
//	c.OnError(func(r *colly.Response, err error) {
//		fmt.Println("Request URL:", r.Request.URL, "failed with response:", r, "\nError:", err)
//	})
//	err := c.Visit(url)
//	if err != nil {
//		fmt.Println("Error Visiting", zap.Error(err))
//	}
//}
