package experiments

import "fmt"

type Repository interface {
	Drive(distance int) string
	ConsumptionCalc(distance int) int
	LeftInTank(distance int) int
}
type Car struct {
	Model         string
	NumPassangers int
	Consumption   int
	FuelTankSize  int
	Engine        string
}

func (c Car) Drive(distance int) string {
	return fmt.Sprintf("\n model: %s;\n engine: %s;\n passed km: %d;\n consumption: %d;\n left in the tank fule: %d\n", c.Model, c.Engine, distance, c.ConsumptionCalc(distance), c.LeftInTank(distance))
}
func (c Car) ConsumptionCalc(distance int) int {
	return (distance * c.Consumption) / 100
}
func (c Car) LeftInTank(distance int) int {
	return c.FuelTankSize - c.ConsumptionCalc(distance)
}
func Less[t comparable](t1, t2 int) string {
	if t1 < t2 {

	}
	return "nil"
}

func StartInterfaces() error {
	car1 := Car{Model: "Skoda", NumPassangers: 5, Consumption: 6, FuelTankSize: 56, Engine: "Diesel"}
	car2 := Car{Model: "BMW", NumPassangers: 5, Consumption: 10, FuelTankSize: 62, Engine: "Diesel"}

	go StartTrip(Repository)
	//go StartTrip(car2, 300)
	//res := fmt.Sprintf(" first trip: %s\n second trip: %s\n", trip1, trip2)
	//fmt.Println(res)
	return nil
}
func StartTrip(repository Repository) {
	repository.Drive(120)
}
