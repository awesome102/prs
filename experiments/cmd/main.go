package main

import (
	"log"
	start "prs/experiments"
)

func main() {
	log.Println("Let's starting Methods")
	err := start.StartMethods()
	if err != nil {
		log.Fatal(err)
		return
	}
	log.Println("Let's starting interfaces")
	err = start.StartInterfaces()
	if err != nil {
		log.Fatal(err)
		return
	}
}
