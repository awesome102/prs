package experiments

import (
	"fmt"
	"math"
)

type Vertex struct {
	X, Y float64
}

func (v Vertex) Abs() float64 {
	return math.Sqrt(v.X*v.X + v.Y*v.Y)
}

func (v *Vertex) Scale(f float64) string {
	v.X = v.X * f
	v.Y = v.Y * f
	return fmt.Sprintf("%f,%f", v.X, v.Y)
}

func StartMethods() error {
	v := Vertex{3, 4}

	//fmt.Println(v.Scale(10))
	//fmt.Println(v)
	fmt.Println(v.Abs())

	return nil
}
