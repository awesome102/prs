package main

import (
	"fmt"
	"os"
	"os/signal"
	"syscall"

	"github.com/caarlos0/env/v6"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"github.com/joho/godotenv"
	"gitlab.com/awesome102/prs/cmd/client/server"
	"gitlab.com/awesome102/prs/cmd/client/telegram"
	"gitlab.com/awesome102/prs/internal/config"
	"gitlab.com/awesome102/prs/internal/prssrvc"
	"go.uber.org/zap"
)

func main() {

	err := godotenv.Load("dev.env")
	if err != nil {
		fmt.Println("unable to load dev.env file: %e", zap.Error(err))
	}
	cfg := config.Config{}

	if err := env.Parse(&cfg); err != nil {
		fmt.Errorf("config loading error", zap.Error(err))
	}
	bot, err := tgbotapi.NewBotAPI(cfg.Telegram.Token)
	if err != nil {
		fmt.Errorf("NEWBOT ERROR", zap.Error(err))
	}

	//bot.Debug = true
	fmt.Printf("Authorized on account %s \n", bot.Self.UserName)
	tg := telegram.NewTgClient(bot)
	if err = tg.Upload(); err != nil {
		fmt.Errorf("Telegram bot err%s", err)
	}
	go func() {
		ch := make(chan os.Signal, 1)
		signal.Notify(ch, syscall.SIGTERM, os.Interrupt)
		<-ch

		//defer cancel()
		fmt.Println("gracefully shutdown sequence started")
		bot.StopReceivingUpdates()

	}()
	handler := server.NewHandler(*tg)
	router := server.MakeRouter(handler)
	fmt.Printf("Authorized on account %s \n", bot.Self.UserName)
	//prssrvc.NewParserCollyBinance("https://p2p.binance.com/en/trade/all-payments/USDT?fiat=UAH")
	//prssrvc.NewParserCollyOkx("https://www.okx.com/ua/p2p-markets/uah/buy-usdt")
	prssrvc.NewParserGoQueryOkx(cfg.Okx.URL)
	if err := router.Run("http://localhost:8080"); err != nil {

	}
	//prssrvc.NewParserGoQueryBinance(cfg.Binance.URL)

}
