package server

import (
	"github.com/gin-gonic/gin"
	"gitlab.com/awesome102/prs/cmd/client/telegram"
)

type (
	Handler struct {
		tgbotcl telegram.Repo
	}
)

func NewHandler(tg telegram.Repo) *Handler {
	return &Handler{tgbotcl: tg}
}
func MakeRouter(h *Handler) *gin.Engine {
	router := gin.Default()
	router.POST("/send", h.sending)
	return router
}
