package telegram

import (
	"fmt"
	tgbotapi "github.com/go-telegram-bot-api/telegram-bot-api/v5"
	"gitlab.com/awesome102/prs/internal/config"
	"log"
	"strings"
)

var keyboardStocks = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("BINANCE"),
		tgbotapi.NewKeyboardButton("OKX"),
		tgbotapi.NewKeyboardButton("BACK"),
	),
)
var keyboardBinance = tgbotapi.NewInlineKeyboardMarkup(
	tgbotapi.NewInlineKeyboardRow(
		tgbotapi.NewInlineKeyboardButtonData("USDT", "[binance,USDT"),
		tgbotapi.NewInlineKeyboardButtonData("ETH", "[binance,ETH"),
		tgbotapi.NewInlineKeyboardButtonData("BTC", "[binance,BTC"),
		tgbotapi.NewInlineKeyboardButtonData("BNB", "[binance,BNB"),
	),
)
var keyboardOkx = tgbotapi.NewReplyKeyboard(
	tgbotapi.NewKeyboardButtonRow(
		tgbotapi.NewKeyboardButton("BUY-OKX"),
		tgbotapi.NewKeyboardButton("SELL-OKX"),
		tgbotapi.NewKeyboardButton("BACK"),
	),
)

type Repo struct {
	cfg config.Config
	bot *tgbotapi.BotAPI
}

func NewTgClient(botAdd *tgbotapi.BotAPI) *Repo {
	return &Repo{bot: botAdd}
}
func (r *Repo) Upload() error {
	u := tgbotapi.NewUpdate(0)
	u.Timeout = 60

	updates := r.bot.GetUpdatesChan(u)

	for update := range updates {
		if update.Message == nil { // ignore any non-Message updates
			continue
		}
		// Create a new MessageConfig. We don't have text yet,
		// so we leave it empty.
		msg := tgbotapi.NewMessage(update.Message.Chat.ID, "")
		//if !update.Message.IsCommand() { // ignore any non-command Messages
		//
		//
		//	continue
		//}

		if update.Message.Text == "binance" || update.Message.Text == "BINANCE" {

			msg.Text = "REQUEST EXAMPLE:\n" +
				"[binance,USDT,UK,GBP,true,[paypal,wise,revolut]]\n\n" +
				"Where :\n" +
				"[ - start of message,\n" +
				"binance - p2p platform,\n" +
				"USDT - asset (or cryptocurrency)\n" +
				"UK - your country, or country where you want take fiat\n" +
				"GBP - fiat \n" +
				"true - only show merchant ads\n" +
				"[paypal,wise,revolut] -  one or several, payment systems or banks\n" +
				"BUY - trade type" +
				"{I know all banks or payment systems which uses binance & okx } \n" +
				"] - end of message"
			msg.ReplyMarkup = keyboardBinance
			_, err := r.bot.Send(msg)

			if err != nil {
				return err
			}

			continue
		}
		fmt.Println("______)******#&@^", update.CallbackData(), "llll")
		if update.CallbackQuery != nil {
			// Respond to the callback query, telling Telegram to show the user
			// a message with the data received.

			fmt.Println("______)******#&@^", update.Message.ReplyMarkup, "llll")
			callback := tgbotapi.NewCallback(update.CallbackQuery.ID, update.CallbackQuery.Data)
			if _, err := r.bot.Request(callback); err != nil {
				panic(err)
			}

			// And finally, send a message containing the data received.
			msg := tgbotapi.NewMessage(update.CallbackQuery.Message.Chat.ID, update.CallbackQuery.Data)
			if _, err := r.bot.Send(msg); err != nil {
				panic(err)
			}

		}
		if update.Message.Text == "okx" || update.Message.Text == "OKX" {
			fmt.Println("okx")
			msg.Text = "REQUEST EXAMPLE:\n" +
				"[okx,USDT,UK,GBP,true,[paypal,wise,revolut]]\n\n" +
				"Where :\n" +
				"[ - start of message,\n" +
				"okx - p2p platform,\n" +
				"USDT - asset (or cryptocurrency)\n" +
				"UK - your country, or country where you want take fiat\n" +
				"GBP - fiat \n" +
				"true - only show merchant ads\n" +
				"[paypal,wise,revolut] -  one or several, payment systems or banks\n" +
				"BUY - trade type" +
				"{I know all banks or payment systems which uses binance & okx } \n" +
				"] - end of message"

			_, err := r.bot.Send(msg)
			if err != nil {
				return err
			}

			continue
		}

		q := strings.Split(update.Message.Text, ",")
		fmt.Println(q)
		fmt.Println(q[0])
		//b := q[0]

		if q[0] == "[binance" || q[0] == "[BINANCE" {
			msg.Text = "VlaDICK XUI"
			_, err := r.bot.Send(msg)
			if err != nil {
				return err
			}
			continue
		}
		if q[0] == "[okx" || q[0] == "[OKX" {
			msg.Text = "Vitya XUI"
			_, err := r.bot.Send(msg)
			if err != nil {
				return err
			}
			continue
		}
		if update.Message.Text == "back" || update.Message.Text == "BACK" {
			msg.Text = "Hi, I'm created for helped you,\n" +
				"safety BUY or SELL cryptocurrency at a best price\n" +
				"on binance.com or okx.com.\n\n" +
				"Please use buttons or key-words:\n" +
				"binance, okx, back"
			msg.ReplyMarkup = keyboardStocks
			_, err := r.bot.Send(msg)
			if err != nil {
				return err
			}
			continue
		}
		// Extract the command from the Message.

		fmt.Println(update.Message.Text, "upds")
		switch update.Message.Command() {
		case "help":
			msg.Text = "I understand:\n" +
				"commands - /buy and /sell." +
				"or "
		case "start":
			msg.Text = "Hi, I'm created for help you,\n" +
				"BUY or SELL cryptocurrency at a best price\n" +
				"on binance.com or okx.com.\n\n" +
				"Please use buttons or key-words:\n" +
				"binance, okx, back"
			msg.ReplyMarkup = keyboardStocks

		case "status":
			msg.Text = "I'm ok."
		default:
			msg.Text = "I don't know that command."
		}
		fmt.Println("HOO-HOO-HOO")

		if _, err := r.bot.Send(msg); err != nil {
			log.Panic(err)
		}
	}
	return nil
}
